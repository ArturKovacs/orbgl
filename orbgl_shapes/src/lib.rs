#![crate_name = "orbgl_shapes"]
#![crate_type = "lib"]

pub mod shapes;
pub mod structs;
pub mod prelude;